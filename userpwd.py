
from random import randint as randomint
import sqlite3
from solution import password_generator, create_user

path = 'user.db'
conn = sqlite3.connect(path, timeout=10)
cur = conn.cursor()

#retrieve data from 10 random users and generate a random 
#password for the user and store it in db against the particualr user
def updateuserpwd():
    for x in range(0,10):
        email = create_user(path)
        length = randomint(6, 12)
        complexity_level = randomint(1, 4)
        pwdgenerate = password_generator(length, complexity_level)
        
        cur.execute("UPDATE userinfo SET Password = ? WHERE Email = ?", (pwdgenerate, email))
        conn.commit()
    
    conn.close()

updateuserpwd = updateuserpwd()       