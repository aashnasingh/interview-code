
from random import randint as randomint
from solution import password_generator, password_complexity_level

#Checks if password generated has the correct complexity level asserted by the 
#method password_complexity_level and print success and fail accordingly.
def passwordchecker(): 
    
    length = randomint(1, 12)
    complexity_level = randomint(1, 4)
    pwdgenerate = password_generator(length, complexity_level)
    l = len(pwdgenerate)
    password_check_level = password_complexity_level(pwdgenerate)
    print(password_check_level)
    if l >= 8 and password_check_level == (complexity_level + 1):
        print('Success') 
    elif password_check_level == complexity_level:
        print("Success")
    else:
        print("Fail")
    
passwordcheck = passwordchecker()