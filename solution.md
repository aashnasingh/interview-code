The solution.py contains 3 methods -
Password_Generator
Password_Complexity_Level
Create_user

The Password_Generator method accepts length and complexity as its parameter and returns a password as a string. 
There were 4 Complexity levels that were defined and for each complexity level random.sample function was used. 
For example, if the complexity is 2, random.sample took input string as a set of all lower case alphabets and  
its length was 1 less than the password length and as it should include atleast one digit, 
this was concatenated with random.sample on the full set of digits and then the string was randomized again. 
After which it produced a psuedo randomized password.

The Password_Complexity_Level accepts a password as its parameter and returns its complexity level. 
For this I used the library re, which is regex for Python. 
For each complexity level I compared it with the defined regex. 
I used the function findall of the re library which returns the pattern it finds in the string and 
if the length of the pattern was equal to the length of the password, it returned the correct complexity level.

For, the Create_User method retrieved a random user by using requests.get method on the api and 
converted the retrieved data into  json format, so that it would be easy to retrieve the name and email from the dictionary. 
After which, did a connection with the Sqlite3 db and stored the data into userinfo table.

The pwdgen.py script contains a passwordchecker method, which calls on the Password_Generator method from solution.py 
with a random complexity level and length passed as parameters. 
The password which is returned which is then passed to the Password_Complexity_Level  method in solution.py,
 where it returns the complexity level of the password.
And if, the complexity level passed and the complexity level returned in the two functions respectively are same 
it prints "Success", according to the length of the password, otherwise it prints "Fail".

The userpwd.py script has a method updateuserpwd which calls the create_user method from solution.py, 
with the db_path passed as parameter. The method is called 10 times to retrieve 10 random users from the api and 
the method returns their email.  After which, the Password_Generator method is called from solution.py, 
which generates a random password and and  update query is used for sqlite database 
to update the password for the particular user according to the email as the email has been set as a unique column.




