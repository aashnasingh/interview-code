# -*- coding: utf-8 -*-

import random  # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
import string
import re
import requests  # https://docs.python.org/3.6/library/string.html

lalphabet_set = string.ascii_lowercase
uaphabet_set = string.ascii_uppercase
digit_set = string.digits
punctuation_set = string.punctuation
pwd = []

def password_generator(pwd_length, pwd_complexity):
    
    if pwd_complexity == 1:
        pwd = random.sample(lalphabet_set, pwd_length)
    elif pwd_complexity == 2:
        pwd = random.sample(lalphabet_set, pwd_length-1) + random.sample(digit_set,1)
    elif pwd_complexity == 3:
        pwd = random.sample(lalphabet_set, pwd_length-2) + random.sample(digit_set,1) + random.sample(uaphabet_set,1)
    elif pwd_complexity == 4:
        pwd = random.sample(lalphabet_set, pwd_length-3) + random.sample(digit_set,1) + random.sample(uaphabet_set,1) + random.sample(punctuation_set,1)
           
    pwd = ''.join(random.sample(pwd,len(pwd)))
    
    return pwd


def password_complexity_level(pwd):
    
    l = len(pwd)
    complexity_pwd_level = 0
    
    if l >= 8 and len(re.findall("[a-z]", pwd)) == l:
        complexity_pwd_level = 2
    elif l>= 8 and len(re.findall("[a-z0-9]", pwd)) == l:
        complexity_pwd_level = 3
    elif len(re.findall("[a-z]", pwd)) == l:
        complexity_pwd_level = 1
    elif len(re.findall("[a-z0-9]", pwd)) == l:
        complexity_pwd_level = 2
    elif len(re.findall("[a-z0-9A-Z]", pwd)) == l:
        complexity_pwd_level = 3
    elif len(re.findall("[a-z0-9A-Z!#$%&'()*+,-./:;<=>?@[\]^_`{|}~]", pwd)) == l:
        complexity_pwd_level = 4
            
    return complexity_pwd_level


def create_user(user):  # you may want to use: http://docs.python-requests.org/en/master/
    
    tabledata = requests.get("https://randomuser.me/api/?results=1&name=first&noinfo").json()
    
    fname = tabledata['results'][0]['name']['first']+ " "+ tabledata['results'][0]['name']['last']
    email = tabledata['results'][0]['email']
    createtable = "CREATE TABLE IF NOT EXISTS userinfo(Name TEXT, Email TEXT unique, Password TEXT)"
    
    conn = sqlite3.connect(user, timeout=10)
    cur = conn.cursor()
    cur.execute(createtable)    
    cur.execute("INSERT INTO userinfo(Name, Email) VALUES(?,?)", (fname,email))
  
    conn.commit()
    
    return email
    


#print(generated_passowrd)
#print(complexity_level)